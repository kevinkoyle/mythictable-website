---
date: '2020-05-21'
title: 'My Mythic Story: Marc Faulise, Part 1'
summary: 'Learn a little about how Mythic Table got started'
---

Hi everyone,

My name is Marc Faulise and I’m the vision holder and one of the founding members of the Mythic Table project.  I’ve been playing some form of Dungeons and Dragons, Pathfinder or some other roleplaying game for over 35 years.  My journey started in Nova Scotia, Canada, when a grade 5 classmate of mine showed me the 1983 revision of the D&amp;D Basic Box set.  I was in love. I still remember my first experience. I made 4 characters: a fighter, a rogue, a cleric and a wizard. Over the next couple of months or years, I threw them against monsters and leveled them all the way through the next three box sets as they were released.  It wasn’t until much later that I was able to form a group of players and I started playing in earnest.

I played all through high school, all through university and afterward.  Eventually, I started moving. I moved to Florida for work, then to Connecticut, to Calgary, Prince Edward Island, even Mexico for a couple of years and eventually to what’s become my new home, Vancouver.  During these transitions, I struggled to find good groups to play with and I missed my old friends and old games. So, when I found d20Pro, Fantasy Grounds and Roll20, I started to see the potential and the possibility that I might be able to play again and revive my love of the game.

This was about 10 years ago, give or take.  These projects were very new. Don’t get me wrong, I loved them and I loved what they were doing, but wow, they were hard to use.  I experimented with a number of them before settling on d20Pro. I built a couple of groups and I taught them how to use the software and we were off.  We played for years and had a great time. Eventually, I met others online that were doing the same thing and they convinced me to give Roll 20 another shot.  I would say it took me 2 years before I was proficient enough with Roll 20 to prefer it over d20Pro, my original love. The deciding factor was Roll20’s play-anywhere functionality.  Being available through the browser was great, but I find myself taking so long to author campaign content. It was frustrating, but it was necessary, so I accepted it. The tool made it easy for me to enhance even my in-person games so I started using it for everything but the frustration never left.  There had to be a better way.

Being a software engineer and a service architect, I was naturally thinking that I could just make a better version of these tools myself, but realistically, these tools are a huge undertaking and, to solve the problems the right way, I would need more than my skill set.  I would need artists, designers, UI engineers and someone to hold them all together. So, my ambitions idled. That is until one day early in January of 2019 when I overheard a number of coworkers complaining about all the problems they were having with existing tools.

“Well, let’s make our own.” I proposed.  It was mostly a joke, but by the end of the day, we had nearly a dozen volunteers.  We even managed to recruit a producer and a user experience designer. Mind you this was a side project commitment.  This basically amounts to 2 to 5 hours a week on average from everyone, but still, this was fantastic momentum. So, we started planning and SVTT was born.

To learn how the team formed, how to we decided on our fundamentals (or pillars) and how we got to where we are today, read [Part 2](/team/mythicstories/marc-2/)
