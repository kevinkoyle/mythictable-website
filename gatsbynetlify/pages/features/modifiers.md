---
date: '2020-08-03'
title: 'Modifier'
summary: 'Modifiers - We Keep Track So That You Do not Have To'
---

# Modifiers

Depending on the rules system modifiers and conditions can be a big deal to manage. Mythic Table endeavors to take the pain away from this process using modern UI techniques we’ve become accustomed to seeing in games.

<img src="/gifs/features/status.gif" alt="Modifiers Gif">

This feature includes the following:
* The ability to add one or more modifiers to a character.
* Time-dependent modifiers.
* Modifiers enabled via actions.
* Modifiers removed via actions.
* Terrain modifiers.
* A simple and intuitive way to view all modifiers on a character.
* Modifier Immunities.
* Standard rule-based modifiers.
* Much much more...
