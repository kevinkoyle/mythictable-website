---
date: "2022-04-02"
title: "Mythic Table Site Map"
summary: "Everything there is to find..."
---

# [Home](/)

- **[/features](/features)**
- **[/land-ack](/land-ack)**
- **[/mission](/mission)**
- **[/ogn](/ogn)**
- **[/privacy](/privacy)**
- **[/roadmap](/roadmap)**
- **[/sitemap](/sitemap)**
- **[/timestamp](/timestamp)**

## [features](/features)

- **[/features/actionsmenu](/features/actionsmenu)**
- **[/features/actionsystem](/features/actionsystem)**
- **[/features/expressive_ui](/features/expressive_ui)**
- **[/features/modifiers](/features/modifiers)**

## [org](/org/index)

- **[/org/ggob](/org/ggob)**
- **[/org/now-non-profit](/org/now-non-profit)**
- **[/org/now-open](/org/now-open)**

## [reports](/reports/index)

- **[/reports/2019](/reports/2019)**
- **[/reports/2020](/reports/2020)**
- **[/reports/2021](/reports/2021)**
- **[/reports/2022](/reports/2022)**

## [finance](/finance/index)

- **[/finance/index](/finance/index)**

## blog

- **[/blog/mythic-table-sleepout-2021](/blog/mythic-table-sleepout-2021)**
- **[/blog/anti-asian-hate-2021](/blog/anti-asian-hate-2021)**
- **[/blog/announcement-2024](/blog/announcement-2024)**

## team

### [mythicstories](/team/mythicstories)

- **[/team/mythicstories/marc-1](/team/mythicstories/marc-1)**
- **[/team/mythicstories/marc-2](/team/mythicstories/marc-2)**
- **[/team/mythicstories/marc-3](/team/mythicstories/marc-3)**
- **[/team/mythicstories/marc-4](/team/mythicstories/marc-4)**
- **[/team/mythicstories/marc-5](/team/mythicstories/marc-5)**
- **[/team/mythicstories/marc-6](/team/mythicstories/marc-6)**

## [ttrpg](/ttrpg/index)

- **[/ttrpg/monster-club](/ttrpg/monster-club)**

## [welcome](/welcome/why)

- **[/welcome/developer](/welcome/developer)**
- **[/welcome/patron](/welcome/patron)**
- **[/welcome/promoter](/welcome/promoter)**
- **[/welcome/why](/welcome/why)**
