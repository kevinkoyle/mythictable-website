---
date: '2020-11-11'
title: 'Mythic Table - First Playable Press Kit'
summary: 'Learn all about our First Playable Release'
---

# Mythic Table

Do you love role playing games? Do you find it difficult to play them online? Do you often
wonder why in this era that there is no online platform that truly empowers the way we
play? Mythic Table aims to be that platform. We are the world’s most ambitious open
source virtual tabletop. Led by industry veterans, we are building the role playing platform
of the future.

## Big News!!!

On November 21st, Mythic Table will proudly announce the release of our First Playable.
This is just a little taste of things to come. We invite you to try it for free today and help us
spread the word.

- https://fp.mythictable.com

## What is Mythic Table?

Mythic Table is more than just another Virtual Tabletop.

**It is a community.** The people that use and make Mythic Table are driven by a need to have 
better tools. We believe that there is a better way to play and we want to own that experience. 
Role playing is important to us and we strive to prove that through our support and our effort.

**It is a modern platform for modern players.** Software development has changed greatly in the 
last 10 years. The expectations of usability and accessibility have changed the way we interact
with our computers and our phones. Mythic Table aims to not only fit into this new environment
but to lead it. With a focus on User Experience, ease of use, and accessibility we aim to set
a new standard for Virtual Tabletops.

**It is a productivity tool.** At its heart, Mythic Table is a tool for productivity. We aim to get
out of the way of the story and we do that by; minimizing the mundane, empowering content 
creation and sharing, automating the basics, trusting the players, and enabling the audit.

## Free? How is that possible?

Yes, 100% free! No paywalls. No blocked features. No marketplace. No limitations.

How? We give our community the option to support us directly through 
systems like Patreon and Kickstarters. We do this only to help pay for our infrastructure
and help us staff full time developers. All of our finances are public. Every penny we 
receive is accounted for. We publish our earnings, our expenses, and our plans and we enable
the community to support us. Do supporters get special treatment? No. At most they will get
an opportunity to help use, test, and QA the product and in the future they might get a 
special badge that celebrates their contribution and support of the product.

## About Us

### Our People

Mythic Table is fully open. Open Source. Open books. Open minds and open hearts. We are
all passionate role players whose lives have been enhanced by the games we love and we
feel it’s time to give something back. Going open for us is a commitment to build something
for the community and to help empower our games and our stories in the future to come.
We see this as a long term commitment at the scale of Chromium and Firefox. We hope
Mythic Table will become a household name in the community of roleplayers across the
world.

- Marc Faulise, Founder
- James Hatheway, Founder
- Sarah Kilby, Director of Communications
- Jon Winsley
- Jose Bonilla
- Lucas Teng
- Carl Gibson
- Adrian Patterson
- Tim Newton
- Santiago Mainetti
- Keith Valin
- Tim Cabbage
- Ankit Sangwan
- Fabiano Alves Duarte
- Edward Austin
- Nikky Piek
- Pete Jones

## Call to Action

We do need help. This is why we have reached out to you. You can help in two ways:

- Delay your announcement to the 21st and provide the link https://fp.mythictable.com
- Announce early and urge your readers to stay tuned for more news
   - Follow this up on the 21st with the link
   - or
   - Urge your readers to follow Mythic Table for further news

## Fact Sheet

### Developer
Mythic Table Foundation,<br/>
Burnaby, BC Canada

### Founding Date
November 21, 2019

### Releases
First Playable<br/>
November 21 2020

### Press/Business Contact
marc@mythictable.com

### Social
- [Mythic Table on Twitter](https://twitter.com/mythictable)
- [Mythic Table on Facebook](https://www.facebook.com/mythictable/)
- [Mythic Table on Reddit](https://www.reddit.com/r/mythictable/)
- [Mythic Table on Youtube](https://www.youtube.com/c/mythictable)

### First Playable in PDF
* [/press/fp.pdf](/press/fp.pdf)
* [All Mythic Table Logo](/press/logos.zip)

### Logos

[![logo-black-white.png](../../images/logos/logo-black-white.png)](/press/logo-black-white.png)
[![logo-white-black.png](../../images/logos/logo-white-black.png)](/press/logo-white-black.png)
[![logo-black.png](../../images/logos/logo-black.png)](/press/logo-black.png)
[![logo-white.png](../../images/logos/logo-white.png)](/press/logo-white.png)
[![logo-long-black-white.png](../../images/logos/logo-long-black-white.png)](/press/logo-long-black-white.png)
[![logo-long-black.png](../../images/logos/logo-long-black.png)](/press/logo-long-black.png)
[![logo-long-white-black.png](../../images/logos/logo-long-white-black.png)](/press/logo-long-white-black.png)
[![logo-long-white.png](../../images/logos/logo-long-white.png)](/press/logo-long-white.png)
[![logo-tall-black.png](../../images/logos/logo-tall-black.png)](/press/logo-tall-black.png)
[![logo-tall-white.png](../../images/logos/logo-tall-white.png)](/press/logo-tall-white.png)
