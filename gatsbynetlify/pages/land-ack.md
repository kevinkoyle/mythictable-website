---
date: '2021-09-30'
title: 'Mythic Table - Truth and Reconciliation'
summary: 'Land Acknowledgement'
---

![Every Child Matters](../images/indigenous/ecm.png)

# We Acknowledge

The Mythic Table platform allows people from all over the world to connect. This digital landscape is new and exciting, but in this time of reconciliation we must not forget how we got here. We recognize that the real lands in which we work, play, grow, and love must be properly acknowledged.

As an organization, we at Mythic Table acknowledge all Indigenous Peoples in Canada and elsewhere in the world. We honour and thank the First Nations, Métis, and Inuit for their ongoing stewardship and protection over the traditional, treaty, and unceded lands on which we come together, whether in-person or virtually. The Mythic Table Foundation was founded in the shared, ancestral and unceded territories of the hən̓q̓əmin̓əm̓ and Sḵwxwú7mesh-speaking people. We respect each of the Nations who share territory in Burnaby. We do this in the spirit of the values that have brought us all together at Mythic Table.

# Why do we do a Land Acknowledgement?

The Land Acknowledgement is a tradition of many Indigenous peoples that goes back centuries. It is used to show respect for the land and those who lived there. These acknowledgments  have been adopted across Canada and are an important part of reconciliation.

For us, land acknowledgments  are an opportunity to reflect on the history of Indigenous people and the impact colonization has had on them. It gives us pause to consider how we can be accountable and thankful to those who live on and care for the land. It is our hope that this small gesture can help move our cultural awareness of these issues and encourage us towards acts of reconciliation.

# How can you learn more and how can you help?

From the Metro Vancouver YWCA:

> “[Y]ou have the responsibility to continue your self-education by asking questions and learning more about reconciliation.
> What privileges do you have on this land because of colonialism? What can you do to better care for the land? Who lived on this land before you? What personal or organizational practices do you > have in place to work towards reconciliation?”

There are many Indigenous peoples who have been displaced by colonialism. Take time to learn about the Indigenous peoples who lived on this land before you. Local groups may have resources or contacts able to share more about Indigenous history, culture and languages. It may be possible to support a language revitalization project local to you.

Those of us in Canada can familiarize ourselves with the Truth and Reconciliation Commission's 94 Calls to Action and raise awareness of how much has been left undone.

https://newsinteractives.cbc.ca/longform-single/beyond-94



