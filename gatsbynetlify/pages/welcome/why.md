---
date: '2020-07-10'
title: 'Why Support Mythic Table?'
summary: 'If you are still unsure about supporting us, read this'
---

# Why Support Mythic Table?

Wondering why you'd want to support Mythic Table over all the other options out there? Here is a list of what we provide that you aren't going to find anywhere else. Also, did we mention that what we are doing is going to be free to our users? 

* [We are 100% open source](/org/now-open/).
* [We are a registered non-profit society](/org/now-open/).
* We are building a modern, expressive UI to get out of the way of your story. Less time doing game prep, more time at the table.
* We will work with publishers to build a digital rights solution to give you access to the content you already own rather than making you buy it on yet another platform.
* Content sharing and reuse will be at the core of our system. 
* You will have the choice to self-host or use our servers for playing your games.
* Mythic Table hopes to change the way product developers work with their customers.

# Believe in Us

If you want help this project grow with your support, read these pages to know how you can contribute:

* [Become a Promoter](/welcome/promoter/)
* [Become a Patron](/welcome/patron/)
* [Become a Developer](/welcome/developer/)
