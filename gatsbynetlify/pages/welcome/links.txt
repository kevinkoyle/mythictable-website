---
date: '2020-07-14'
title: 'Hotlinks'
summary: 'Hotlinks reference page'
---

These are the places where the files from this folder are hotlinked except the ones linked to the index.md file in this folder. These are referenced here in case one needs to move or rename one of the files in here.

why.md -> /gatsbynetlify/pages/org/index.md

developer.md -> /gatsbynetlify/pages/mission.md
patron.md -> /gatsbynetlify/pages/mission.md
promoter.md -> /gatsbynetlify/pages/mission.md
why.md -> /gatsbynetlify/pages/mission.md

why.md -> /gatsbynetlify/pages/welcome/developer.md
why.md -> /gatsbynetlify/pages/welcome/patron.md
why.md -> /gatsbynetlify/pages/welcome/promoter.md

developer.md -> /gatsbynetlify/pages/welcome/why.md
patron.md -> /gatsbynetlify/pages/welcome/why.md
promoter.md -> /gatsbynetlify/pages/welcome/why.md
