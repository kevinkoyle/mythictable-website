---
date: '2020-07-10'
title: 'Become a Developer'
summary: 'How You Can Support Us With Your Skills as a Developer'
---

# We need your help!

Mythic Table would be nothing without its wonderful and passionate development team. If you want to join this amazing team, please get in contact with us to get involved and build something incredible with us.  You can email us at support@mythictable.com, or you can jump on our Discord Server and let us know you're interested.

These are the roles we're looking to fill:

* Software Engineers (Vue/.net)
* DevOps Engineers
* Designers
* Artists
* Community Managers
* Project Managers

## Beginners Welcome

Are you a beginner in any of the above roles and open to learning more? Mythic Table is the perfect project to acquire hands-on experience in working on a customer facing application that utilizes modern development practices.

By participating in this open source project you'll gain valuable experience. Here are just a few of the opportunities you'll have by being part of the Mythic Team:

* Learn new skills
* Build a product in an early stage
* Network with a global remote team of developers
* Be part of a growing community

If you are still unsure about supporting us, please read this to learn more:
* [Why support Mythic Table?](/welcome/why/)
