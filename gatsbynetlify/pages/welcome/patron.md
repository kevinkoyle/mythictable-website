---
date: '2020-07-10'
title: 'Become a Patron'
summary: 'How you can support us financially as a Patron'
---

# We need your help!

Mythic Table relies on the volunteer contributions of many passionate developers. But in order for us to build the best product we can, we need full time developers. 

You can help this effort by supporting Mythic Table financially on Patreon. We couldn't do what we do without out incredible backers. Your contribution will go toward hiring a dedicated developer to work full-time on Mythic Table and covering the infrastructure costs.

Just click here to become our Patron: 
* [https://www.patreon.com/mythictable](https://www.patreon.com/mythictable)

If you are still unsure about supporting us, read this to know more:
* [Why support Mythic Table?](/welcome/why/)
