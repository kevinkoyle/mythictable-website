---
date: "2022-04-18"
title: "Roadmap"
summary: "Plans for the Future"
---

# Roadmap

We appreciate your enthusiasm for Mythic Table's future, but unfortunately our product roadmap is currently under construction and we wouldn't want to present an under-developed roadmap. Please keep checking back or follow our <a href="#footer">socials</a> for continued updates!

  <div class="video">
              <iframe src="https://www.youtube.com/embed/x9Pof_NIK1Y"
                frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" title="Roadmap Townhall"
                allowFullScreen style="width:100%; height:80vh;" ></iframe>
  </div>
