---
date: "2021-11-06"
title: "Mythic Table Financial Statements"
summary: "A list of all Financial Statements avialable from Mythic Table"
---

## Financial Statements

### Income Statements

- 2019
  - [Overall](/finance/2019-income.pdf)
- 2020
  - [December](/finance/202012-income.pdf)
- 2021
  - [January](/finance/202101-income.pdf)
  - [February](/finance/202102-income.pdf)
  - [March](/finance/202103-income.pdf)
  - [April](/finance/202104-income.pdf)
  - [May](/finance/202105-income.pdf)
  - [June](/finance/202106-income.pdf)
  - [July](/finance/202107-income.pdf)
  - [August](/finance/202108-income.pdf)
  - [September](/finance/202109-income.pdf)
  - [October](/finance/202110-income.pdf)
  - [November](/finance/202111-income.pdf)
  - [December](/finance/202112-income.pdf)
- 2022
  - [January](/finance/202201-income.pdf)
  - [February](/finance/202202-income.pdf)
  - [March](/finance/202203-income.pdf)
  - [April](/finance/202204-income.pdf)

### Balance Sheets

- 2019
  - [Overall](/finance/2019-balance.pdf)
- 2020
  - [December](/finance/202012-balance.pdf)
- 2021
  - [January](/finance/202101-balance.pdf)
  - [February](/finance/202102-balance.pdf)
  - [March](/finance/202103-balance.pdf)
  - [April](/finance/202104-balance.pdf)
  - [May](/finance/202105-balance.pdf)
  - [June](/finance/202106-balance.pdf)
  - [July](/finance/202107-balance.pdf)
  - [August](/finance/202108-balance.pdf)
  - [September](/finance/202109-balance.pdf)
  - [October](/finance/202110-balance.pdf)
  - [November](/finance/202111-balance.pdf)
  - [December](/finance/202112-balance.pdf)
- 2022
  - [January](/finance/202201-balance.pdf)
  - [February](/finance/202202-balance.pdf)
  - [March](/finance/202203-balance.pdf)
  - [April](/finance/202204-balance.pdf)

### Financial Reports

- 2021
  - [March](/finance/202103-financial-report.pdf)
