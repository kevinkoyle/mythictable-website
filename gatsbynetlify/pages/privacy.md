---
date: '2020-07-20'
title: 'Our Privacy Policy'
summary: 'The foundation of your trust in us'
---

# Privacy Policy

Mythic Table is part of the Mythic Table Foundation which includes the Mythic Table Website and the Mythic Table Platform. This privacy policy will explain how our organization uses the personal data we collect from you when you use our services.

Your privacy is important to us and we take our responsibility of caring for it seriously, this policy is not written by lawyers so that only other lawyers may understand it, it is written by the Team at Mythic Table so it would be easily understandable to our fans. The information collected by Mythic Table depends on what Services you use and how you use them.

<!-- # Topics

1. [Privacy Policy](#privacy-policy).

    1. [What data do we collect?](#what-data-do-we-collect)

    1. [How will we use your data?](#how-will-we-use-your-data)

    1. [How do we store your data?](#how-do-we-store-your-data)

    1. [What are your data protection rights?](#what-are-your-data-protection-rights)
    
    1. [How to exercise your rights?](#how-to-exercise-your-rights)

1. [Cookies](#cookies)

    1. [How do we use cookies?](#how-do-we-use-cookies)

    1. [What types of cookies do we use?](#what-types-of-cookies-do-we-use)

    1. [How to manage cookies](#how-to-manage-cookies)

1. [Privacy policies of other websites](#privacy-policies-of-other-websites)

1. [Changes to our privacy policy](#changes-to-our-privacy-policy)

1. [How to contact us](#how-to-contact-us)

1. [How to contact the appropriate authority](#how-to-contact-the-appropriate-authority) -->
 
# What data do we collect?

Mythic Table collects the following data:

- Personal identification information (email address).
- Your Mythic Table account information, including your email address, username and password.
- Details provided by you in response to surveys or contests.
- Information provided by you when you seek help from us, such as your name, surname, email address.
- (Patreon) Billing information such as your name and payment details/we don’t collect payment details directly (see below/read patreon PP).
- Information you provide when playing online.

# How do we collect your data?

You directly provide Mythic Table with most of the data we collect. We collect data and process data when you:

- Register online or place an order for any of our products or services.
- Voluntarily complete a customer survey or provide feedback on any of our message boards or via email.
- Use or view our website via your browser’s cookies.
- Sign up for newsletters or updates.
- Contact mythic table directly via email (Name, surname, email address).

Mythic Table may also receive your data indirectly from the following sources:

- Facebook
- Twitter
- Reddit
- YouTube

# How will we use your data?

Mythic Table collects your data so that we can:

- Send updates and news.
- Allow for password resets.
- Send important notifications.
- Analyze the usage of our services through platforms like Google Analytics.
- Operate, improve and develop our games and services.
- Serve and measure the effectiveness of advertising.
- Set up and maintain your accounts.
- Detect security incidents, protect against malicious, deceptive, fraudulent or illegal activity.
- Provide game an excellent experience.
- Identify, fix, and troubleshoot bugs and service or functionality errors.
- Provide software updates.
- Run competitions and contests.
- Other purposes you consent to.

If you agree, Mythic Table will share your data with our partner companies so that they may offer you their products and services.

- Mailchimp
- Slack
- Discord
- Google Analytics
 
# How do we store your data?
 
Mythic Table securely stores your data in Google Cloud via Mongo Atlas and in the Mailchimp service.

Mythic Table will keep your personal data for the duration of your use of the service. Once this time period has expired, we will delete your data by removing it from our datastores. Data kept on Mailchimp can be managed with your subscription to the newsletter. Any and all data can be removed upon request.


# What are your data protection rights?

Mythic Table would like to make sure you are fully aware of all of your data protection rights. Every user is entitled to the following:

#### The right to access 

You have the right to request Mythic Table for copies of your personal data. There is no fee for this service but you will be required toverify your account.


#### The right to correction

You have the right to request that Mythic Table correct any information you believe is inaccurate. You also have the right to request Mythic Table to complete the information you believe is incomplete.


#### The right to erasure

You have the right to request that Mythic Table erase your personal data, under certain conditions.


#### The right to restrict processing

You have the right to request that Mythic Table restrict the processing of your personal data, under certain conditions.


#### The right to object to processing

You have the right to object to Mythic Table’s processing of your personal data, under certain conditions.


#### The right to data portability

You have the right to request that Mythic Table transfer the data that we have collected to another organization, or directly to you, under certain conditions.
 
# How to exercise your rights?

If you would like to exercise any of these rights, please contact us at our email: [support@mythictable.com](support@mythictable.com). If you make a request, we have one month to respond to you. 

# Cookies

Cookies are text files placed on your computer to collect standard Internet log information and visitor behavior information. When you visit our websites, we may collect information from you automatically through cookies or similar technology. We use cookies and similar technologies to help us understand how you use our services. They are also used to help us understand your preferences based on previous or current site activity.


For further information, visit [AllAboutCookies](allaboutcookies.org).


# How do we use cookies?

Mythic Table uses cookies in a range of ways to improve your experience on our website, including:

- Keeping you signed in
- Analyzing website usage
- Understand and save user’s preferences

# What types of cookies do we use?

There are a number of different types of cookies, however, our website uses:

#### Functionality

Mythic Table uses these cookies so that we recognize you on our website and remember your previously selected preferences. These could include what language you prefer and location you are in. A mix of first-party and third-party cookies are used.
 
#### Analytics

Mythic Table uses cookies to assist with tracking activity on our website. This is important for us to understand how people are using our services and where we can make improvements.


# How to manage cookies

You can set your browser not to accept cookies, and the above website tells you how to remove cookies from your browser. However, in a few cases, some of our website features may not function as a result.

# Privacy policies of other websites

The Mythic Table website contains links to other websites. Our privacy policy applies only to our website only, so if you click on a link to another website, you should read their privacy policy.

# Changes to our privacy policy

Mythic Table keeps its privacy policy under regular review and places any updates on this web page. This privacy policy was last updated on 12 June 2020.

If you (our fans) feel like we’ve excluded something or have any suggestions for updates please email us at [support@mythictable.com](support@mythictable.com) and we will evaluate and update accordingly. Your opinion matters and we want all our fans to know that they are in a safe place.

# How to contact us

If you have any questions about Our Company’s privacy policy, the data we hold on you, or you would like to exercise one of your data protection rights, please do not hesitate to contact us.

Email us at [support@mythictable.com](support@mythictable.com)
 
# How to contact the appropriate authority
 
Should you wish to report a complaint or if you feel that Mythic Table has not addressed your concern in a satisfactory manner, you may contact the Information Commissioner’s Office.

Website: https://ico.org.uk/

Number: 0303 123 1113 or [live chat](https://ico.org.uk/global/contact-us/live-chat/).

