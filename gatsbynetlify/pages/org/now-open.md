---
date: '2019-09-08'
title: 'Now Open-source'
summary: 'We are now officially Open-source!'
---

### We are very proud to announce that Mythic Table is now Open Source and Open Books.  

You can find our code repository here: https://gitlab.com/mythicteam/mythictable

We have chosen to work with Gitlab because we believe strongly in DevOps practices, automation, and testing. Gitlab's pipelines give us all of these.  Also, the people at Gitlab have been fantastic in their support for this project, and we're delighted to work with them.

### What about service hosting?

We will continue to host Mythic Table on our servers and offer it free to everyone.  All features will be free without subscription.  There will be no signup cost, and there will be no paywalls.  There will be an option to subscribe, but this will be strictly optional.  Subscriptions will be used to help fund the cost of our development and infrastructure.  All of these costs and this income will be fully disclosed in our reports so you can see how we put your contribution to work.  This will be discussed more below.

### But why?

There are a number of reason for this:

* We strongly believe in being open and honest with our community in every way
* We feel this is the best way for a small dedicated team to build the product that meets everyone's needs
* We hope that if we are up front about everything and that you believe in us that you will help us succeed

The truth is that no one in this space is cashing out a billionaire. We're here because we love the games we play and we love the work we do to bring them to life. Many of the ideas we have about running this project are not new. In fact, if you're interested in learning more you can read a bit about the [BOOK](/org/ggob/) that inspired us in this direction.

### How do I contribute?

There are two ways to contribute: time and money.  If you're a developer and you have some skills that we're looking for, reach out to us and let us know you're interested in helping.  If you're not a developer and you believe in our vision, you can contribute by helping us fund this project.  More about this later as well.

### What's next?

Now that we're live on Gitlab, we will need to put some policies and procedures in place for those of you that may wish to contribute directly to the project.  This process will take some time and will likely change as we receive feedback from those that we've on-boarded. We're planning on testing this process on our early adopters, and once we have it formalized, we will roll it out for everyone.  That said, we don't expect everyone to contribute in this way.  The majority of the development will be handled by the core team and by paid contractors.

To open the books, we will publish a periodic report.  These reports will focus on our forecast and how we are tracking to these plans.  The goal is to enable us to change and pivot in response to unexpected developments.  Signing up for the newsletter will allow you to get these reports right in your inbox, but we will also publish them on the website.

### How is this going to work?

This is probably the question that brought you all here.  Anyone that knows anything about business understands that you can't just go around giving away your product.  It takes time and money to make tools like Mythic Table.  At the very least you need to make that money back.  Ideally, you would make enough to pay off loans, to build cash flow for things like payroll, insurance, and rent and to reinvest.  Traditional businesses sell or rent products and services. This income is designed to generate value;  value to your customers, value to your employees, and value to your stakeholders.

Mythic Table's approach is quite different.  We expect a small percentage of you to be willing to support the project with a contribution that you feel you can easily afford without impacting your quality of life.  Our goal is to use this contribution to cover the cost of development, infrastructure, and to fund improvements.  At the same time, we shall report on our finances and our progress to maintain transparency and trust.  This financial reporting is essential for several reasons.  It establishes trust, which is extremely important with any stakeholder, and it will also let you know when things are going poorly.  If for any reason we are not making our targets, we need to be forthcoming so that everyone knows and understands why.  Feedback from this report is how we will come up with solutions to any problems and set-backs and in the worst-case scenario, how we will explain changes in our milestone deliveries.  If we can count on all of you to take part in Mythic Table as owners, we believe that we cannot fail.

That is why we say that we are building a community of owners, not customers.  This approach is a fundamental distinction, and it's the difference between other organizations and us.  This difference is also one of the main driving forces behind my continued involvement in this project.  Yes, I would love it to be successful.  And I strongly believe in our usability vision.  However, I know this business model has potential, and this is a fantastic opportunity to prove it.

### Specifically, how will this work?

At the writing of this article, I am in the process of working with a new User Experience Artist.  Together we are designing a series of feature videos to demonstrate our vision.  Our goal is to get these videos ready, so we can generate some interest in Mythic Table's unique user experience target.  We will use them to attract more people to the project.  We hope to use a Kickstarter to fund the first playable and get Mythic Table's business model fully functional.  We expect we will need 3000 fully engaged members to raise enough to fund the development costs, and we are significantly lower than that at this time.  These UI mockup videos will go a long way to attracting more members.

During this, I will be working fulltime.  My role will be to plan, organize, and to handle messages like these.  We will employ contractors to do the development.  Like our new User Experience Artist, these contractors will be paid out of a somewhat limited budget.  Currently, this is my personal finances, but hopefully, we'll be in a position to accept contributions from all of you.  Our forecasts show what that contribution needs to be and how long it will take us to develop features.  These forecasts will be published weekly or every two weeks.  They will show all of you how we're trending compared to our plan.  

Once we've built our first playable, we should be well underway. We'll have proved our reporting models and worked out any wrinkles therein. We'll have a healthy community with which we will be getting predictable contributions. We'll have built our technology and a team of contractors on whom we can depend.  Finally, we'll have a forecast and a plan that reflects our new landscape.  But we won't be done. That's when our real work will begin as we work to deliver the User Experience we envision.

So, in conclusion, I've outlined our journey. Not my journey. All of our journey.  Please, join me and let's build Mythic Table with pride.

--Marc
