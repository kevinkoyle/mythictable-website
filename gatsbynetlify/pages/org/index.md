---
date: "2020-07-14"
title: "Our Philosophy"
summary: "These are articles published by us which outline our philosophy"
---

Every organisation has some principles which form its base. As you know Mythic Table is a Open Books, Open Source and Non Profit organisation. So what does it all mean and how did Mythic Table come to be this way. All the answers are in the articles below.

- [The Great Game of Business - Our Inspiration](/org/ggob/)
- [Going Open - What it means for Mythictable](/org/now-open/)
- [Becoming a Non Profit Organization - Our commitment towards openness](/org/now-non-profit/)
- [Why Support Mythic Table?](/welcome/why/)
