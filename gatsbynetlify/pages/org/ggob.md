---
date: '2020-01-01'
title: 'The Great Game of Business'
summary: 'The inspiration behind Mythic Table Openness'
---

Mythic Table's Open Source strategy is based heavily on the teachings found in Jack Stack's book, The Great Game of Business.  I read this book while in the very early stages of this project.  At that time, we were looking at Mythic Table like any other traditional business.  We would need startup capital, a prototype, engaged customers, and a plan that shows a positive return on investment. I was lucky enough to fail at this because otherwise, I would not have looked for alternatives.  I wouldn't have thought to apply the Great Game of Business in this unconventional way.

#### So, what is the Great Game of Business?

Jack Stack does a better job of explaining this than I could, so get his book.  It's short.  If you commute to work as I do, get the audio.  He narrates it himself, and he does a great job.  To summarize, the Great Game of Business or the GGoB is a management philosophy meant to turn employees into owners.  First, it uses an education program to teach employees how the business works.  Then it opens the books so employees can see how things are going. Finally, it introduces a bonus program that rewards everyone if the company exceeds its targets. The idea works well because it engages everyone in the decision-making process and gets focuses them on what's important.  This is carried with them in their day to day work. As they make the small decisions that make up their job, they do so knowing what's truly important to the organization.

The GGoB talks extensively about the dangers of opening the books.  What will happen if competitors take advantage of the information therein?  What will happen people realize how we're losing money?  What will they do if they see we're making money?  Some of these concerns are not trivial.  There could be a real-world impact, but Jack Stack claims that most people will already be telling themselves stories about these numbers and more often than not, they will be greatly exaggerated.  It's better to be upfront about the finances and trust your people to stay with you through the good times and the bad.

Mythic Table hopes to do something similar.  Instead of turning employees into owners, we aim to turn customers into owners.  That's you.  We'll do this by using the same model described in the GGoB. We'll educate you on how our Mythic Table works as a business.  We'll report our finances to you on a regular basis.  Finally, we'll share with you our forecast, plans, and projections with a hope that together we can find solutions to any problem we encounter.

So, what will happen to us when we open the books?  Will competitors take advantage of the information?  Maybe, and I hope they do.  I've always said that I enjoyed the earlier virtual tabletop and I wish them well.  In truth, the biggest thing that we're doing differently than them is this strategy.  I hope they all adopt it as well.  

What will happen the community realizes how we're losing money? Well, we're already doing that.  But seriously, I hope that all of you will pitch in to do what you can to stop that from happening.  What will the community do if they see we're making money?  Well, I expect two things will happen.  As we are dependant on voluntary contribution, I think the level of that contribution will naturally drop, but I hope not.  Instead, I hope that we can rally around plans to reinvest and add features we all want.

Using the Great Game of Business in this way has never been done before as far as I know.  There will be challenges, but if we can overcome them, we can realize something magnificent.  We can build Mythic Table, and it will be ours.

I'm proud that all of you have put your faith in me and that you're willing to join me on this journey.  Thank you for your continued support.
