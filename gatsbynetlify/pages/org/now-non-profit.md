---
date: '2019-11-24'
title: 'Mythic Table - Non-profit'
summary: 'We are now officially a not-for-profit foundation!'
---

# Mythic Table Incorporates as a Not-for-Profit.

Mythic Table is an Open Source online platform for tabletop role-playing games. We are focused on creating a Virtual Tabletop with a modern user experience, unique usability features, and high performance and flexibility. We are happy to say we will be hosting this service free of charge. No features will be locked behind paywalls. Our goal is to make a product that outstrips what’s available right now and offer it for free.

#### Mythic Table’s Founder and key Vision holder, Marc Faulise, says;

“When we thought about what kind of organization we wanted to make to manage this project, Not-for-profit just seemed like the natural fit. When I brought this up with the team, there was a lot of excitement. For me, being non-profit means we can focus on what's important and any advantage we get can be immidately turned around and given back to the community in the form of better features, stability and content.”

That said, we are excited to announce that we have now successfully incorporated as a Society in British Columbia, Canada. We would also like to announce the creation of our Board of Directors: Sarah Kilby, James Hathaway, and Marc Faulise. Our team is made up of volunteers from all over the world and we are actively looking for help from developers or artists who can give their time to our cause. For those interested in joining us in developing Mythic Table, please join our newletter and follow the instructions in your welcome email.

#### Our Non-profit Incorporation Certificate
* [Non-profit Certificate](/biz/certificate-non-profit.pdf)

#### We are also accepting monthly or one-time donations via a Patreon page. Your donation will help us get our first playable version of Mythic Table out the door. Please visit
* https://www.patreon.com/join/mythictable

#### Our social media accounts:
* https://www.reddit.com/r/mythictable/
* https://twitter.com/mythictable
* https://www.facebook.com/mythictable/
* https://mythictable.com/
