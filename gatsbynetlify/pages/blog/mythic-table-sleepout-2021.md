---
date: '2021-02-20'
title: 'Mythic Table Sleepout'
summary: 'Mythic Table Sleeps out for Charity'
---

Hey there! Welcome to the Mythic Table Blog! Here we hope to share our thoughts and experiences regarding a different subject each week. 

This time we would like to share our experience doing a “sleep out” (sleeping out in the open air) for charity. It was late November, 2019, and the goal was to raise critical funds and awareness for youth experiencing homelessness. Our hearts were touched by this cause, so we gave it our best shot!  

There were three of us representing the Mythic Table team: Sarah Kilby (Myself), James Hatheway and Marc Faulise. We tried to document as much of this experience as we could. 

This was the space that James had to work with for the night out:

<table>
  <tr>
    <td><img src="../../images/blog/image4.jpg" width=170 height=380></td>
    <td><img src="../../images/blog/image3.jpg" width=270 height=480></td>
  </tr>
 </table>

 <table>
  <tr>
    <td><img src="../../images/blog/image2.jpg" width=370 height=480></td>
    <td><img src="../../images/blog/image5.jpg" width=270 height=480></td>
    <td><img src="../../images/blog/image7.jpg" width=270 height=480></td>
  </tr>
 </table>


James says it helped that, “My wife packed me in like a burrito”. 

The weather overnight in Vancouver, BC, Canada (Where Marc and James live) started at 7℃ (44℉) but dropped to 3℃ (37℉) overnight. I had it easier. The weather in Oshawa, ON, Canada (Sarah’s) was a relatively mild 10℃ (50℉)! It was quite cold to be sleeping outside, but it was doable..

My (Sarah's) fiancė also insisted on me upgrading to a twin mattress for the back deck where I camped out. Instead of using words to paint you a picture, here are some actual pictures!


![Image 11](../../images/blog/image11.jpg)


 <table>
  <tr>
    <td><img src="../../images/blog/image8.jpg" width=370 height=480></td>
    <td><img src="../../images/blog/image6.jpg" width=270 height=480></td>
  </tr>
 </table>

I’d have to say that Marc probably went through the most realistic experience that a homeless person might go through overnight in Vancouver. He started off with just a blanket and a thin piece of cardboard to lay on. And yes, fifteen minutes in he was like “**** this, I’m getting a pillow” (then proceeds to produce the world’s smallest pillow). Even with that “luxury” he did an admirable job of putting himself into someone else's shoes. Kudos Marc. A quote from Marc: “So cold. So uncomfortable. Can't sleep”.


 <table>
  <tr>
    <td><img src="../../images/blog/image1.jpg" width=370 height=480></td>
  </tr>
 </table>


On a final note, we will be doing another Sleepout Under the Stars to support this cause. Donations are being accepted now until February 25th, 2021, the date of the sleepout. 

Also huge news! Any donations given, up until the sleepout, will be doubled. Co-Pilot Properties Ltd. will match any donation you make by February 25 with an equal donation of their own. They are matching up to $35,000!
 
Here is the link to donate or to join Team Mythic Table and sleepout for charity with us: [Sleep Out: Professionals Edition: Mythic Table - Covenant House Vancouver (convio.net)](https://secure3.convio.net/chb/site/TR/Events/FY21SleepOutProfessionalsEdition?team_id=2688&pg=team&fr_id=1351)

Your $30 gift becomes $60 and will provide six youth with a nutritious breakfast, lunch, and dinner.
Your $125 gift becomes $250 and will provide mental health supports for one youth for two weeks.
Your $360 gift becomes $720 and will provide two youth with warm beds and 24 hours of care in the Crisis Program.

Please help us raise critical funds for young people experiencing homelessness.

Our generous donors so far: 
* Silvia and Peter
* MP
* Carolyn
* Tyler W
* World Changing Kindness
* East Side Games
* Mr. James Hatheway
* Jessie
* Luis Placid
* Mr. Jose Luis Bonilla
* Visar
* Karen Sawyer
* Anonymous
* Blinky
* Anonymous
* Bernard Bernoulli, Esq
* Arlene Andrews

Thank you for checking out our first blog post! Please let us know what you want to hear about in our next post. Just message us on our Facebook page: https://www.facebook.com/mythictable

Thank you!


Sarah K\
Mythic Table\
www.mythictable.com
