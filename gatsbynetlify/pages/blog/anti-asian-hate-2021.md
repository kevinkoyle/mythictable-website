---
date: '2021-04-12'
title: 'Statement on Anti-Asian Hate Crimes'
summary: "Mythic Table's Statement on Anti-Asian Hate Crimes"
---

Over the past year, racially motivated hate crimes against Asian Americans and Asians globally have increased almost 150%. 

> _We at Mythic Table stand in sorrow and solidarity with Asian communities globally and in particular with those affected by the increasing racist attacks in America. We condemn these attacks and the hatred that fuels them. We mourn with the global Asian community the lives that were lost and continue to dedicate ourselves to fostering a diverse community at Mythic Table._

The Atlanta shootings last month and the increasing Anti-Asian hate crimes across America are a sad reminder that racism persists in 2021. The recent—and increasing—attacks on Asian Americans have spurred Asian victims of hate crimes to speak up. But it’s not just an American problem.

**It’s a global problem.**

As people have been unifying across the globe to stand in solidarity with Asian and Pacific Islander victims of hate crimes, the incidents of racism based hate crimes have increased at an alarming rate. It’s time to say enough is enough. Racism ends here.

I am proud to be able to say that Mythic Table is a diverse and equal-opportunity organisation. We don’t talk about it much because we live it. Our team members come from five continents and our community originate in over 94 countries. We are united by our passion for tabletop role-playing games and our dedication to delivering a quality virtual tabletop. That passion and dedication looks the same no matter if it comes from Asia, the Pacific, Africa, Europe or the Americas.

We acknowledge and embrace our global citizenship. We are proud of our diversity and share a deep commitment to respect all our community, no matter what race, gender, religion, ethnicity or sexual orientation.

Some of our very own team members have been victim to anti-Asian hate as a result of the COVID-19 pandemic. So we understand firsthand the difficulties and challenges that our Asian communities face on an increasing basis every day.

Now is the time to unite and stand against racism. If you are experiencing hatred in your community, we want you to know you are not alone. You can speak up. Together, we will make a stand against racism.

Here are three things you can do to help the Asian people in your life:

1. Read and learn about the history of Anti-Asian hate. This article by CNN (https://edition.cnn.com/2021/03/05/us/how-to-support-the-aapi-community-iyw-trnd/index.html) is a good starting point
2. Report Anti-Asian Hate crimes and donate to an Asian support organisation in your local area. The Asian American Commission in USA is a good starting point. (https://www.aacommission.org/resources/anti-asian-hate-resources/)
3. Check on the Asian people in your life and support local Asian business owners who have been hit the hardest.


Let’s stand together and unite against Anti-Asian hate and bring people together again one roll at a time.


Roll High.

Linda Spencer