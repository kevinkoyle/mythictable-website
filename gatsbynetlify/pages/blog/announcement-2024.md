---
date: "2024-05-08"
title: "Mythic Table - Absence and Update"
summary: "Why have we been so quiet at Mytic Table"
---

# Dear Mythic Table Community

I've been absent, and I owe you all both an explanation and a heartfelt thank-you. Your patience and unwavering support during this time have been inspiring. Your loyalty fuels our drive to excel.

Recently, life threw some formidable challenges my way. The toll of the COVID-19 pandemic, the loss of a dear friend, the end of a significant relationship, and the devastating news of my father's battle with cancer—it all felt like an impossible wave crashing down on me. Amidst this turmoil, I struggled to find my footing personally and professionally.

However, adversity can reveal one's values. Guided by the principles of the Mythic Table—transparency and integrity—I have decided to share my journey with you, no matter how difficult it may be.

Amid the chaos, there came a pivotal moment of clarity. I realized that I had been chasing the wrong priorities, and allowing my professional aspirations to overshadow my well-being. It took me a series of setbacks, including a layoff, to reassess and reprioritize.

In collaboration with my dedicated team, we took decisive action to streamline our operations (cutting our infrastructure costs by ¾), and significantly reducing our costs. (An immense pressure that we've been under is our costs vs. our income because we decided long ago to keep Mythic Table available for free; that's why our generous Patrons continue to be vital to our success.) This newfound efficiency breathed life into our efforts, reigniting my passion for our shared vision.

Behind the scenes, we've been working hard, laying the groundwork for exciting new features like the Mythic Table Library. With plans solidified and progress underway, I'm thrilled to announce that we're officially back in action.

Of course, none of this would be possible without the steadfast commitment of our team members—Keith (Tech Lead), Jose (DevOps), Lucas (UI Lead), Kevin (Director of Communication), Sarah (Director of Production), and others—who have remained devoted throughout.

Here are our goals to begin our recommitment:
1. Restart our Kickstarter work in an attempt to finish off these commitments
1. Start a cadence of reviewing and onboarding new volunteers
1. Start looking for new sources of funding again
1. Get more involved with you guys again
1. Address the needs of our Patrons

As we embark on this new chapter, I would like to express my deepest gratitude for your unwavering support. We pledge to redouble our efforts, striving to earn your trust anew as we continue to evolve and refine Mythic Table into the best it can be.

Thank you for believing in us.<br/>
Sincerely,<br/>
Marc Faulise<br/>
Mythic Table, Director
