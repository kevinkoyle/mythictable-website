---
date: "2022-04-27"
title: "Quest"
summary: "Join the Mythic Table Adventure"
---

# Join the Team

Do you want to join the team and help build something exciting and fun? Do you have a skill you want to show off by contributing to the Mythic Table product? Are you new to coding and need real experience to land your first job? If you answered yes to any of these, you should fill out the form below. We would love to have you on the team.

<iframe src="https://forms.monday.com/forms/embed/83213edb05246501663f8334bd260876?r=use1" width="650" height="1000" style="border: 0; box-shadow: 5px 5px 56px 0px rgba(0,0,0,0.25); margin:0 auto; display:block;"></iframe>
