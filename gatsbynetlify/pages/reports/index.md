---
date: "2021-01-31"
title: "Mythic Table Reports"
summary: "A list of all the year for which reports are avialable of Mythic Table"
---

## [Monthly Reports](/reports/index)

The reports are arranged by years and months. To access the reports from a particular year, please choose the year below:

- [Reports for Year 2019](/reports/2019/index/)
- [Reports for Year 2020](/reports/2020/index/)
- [Reports for Year 2021](/reports/2021/index/)
- [Reports for Year 2022](/reports/2022/index/)

## [Financial Statements](/finance/index)

- A complete list of financial statements can be found at **[/finance/index](/finance/index)**
