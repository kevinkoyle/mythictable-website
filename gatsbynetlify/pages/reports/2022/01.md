---
date: "2022-01-31"
title: "Mythic Table January 2022 Report"
summary: "Where we are and where we are going"
---

## The Mythic Table January 2022 Report!

Greetings, **Adventurer!**

Please, pull up a seat at the table. We've had a bit of a slow start to the month, but as we move along through Mythic Table has had a very contemplative month. We've made some important decisions we hope will strengthen our infrastructure moving forward.

First, we will be putting some effort toward creating an improved recruitment strategy as well as in developing new onboarding tools to ensure everyone who wishes to lend a hand has the tools necessary to make that experience the best it can be. We'll also be working on improving our social media processes and expanding our social activity.

We've also found a new victim—_err, I mean_…developer. **Welcome to Blaine!** Going by UnCaged1 on Discord, Blaine will be joining the team with a focus on web development; his first initiative will be helping us work toward freshening up our public facing website. We're really excited to share a wonderful 2022 with you and can't help but thank you again for your continued support!

## Achievements This Month

- Community Contributions.
  - Generating new interest from community members interested in helping get involved. This will allow us to ramp up our web development and QA!
- DevOps Improvement.
  - It’s now running and cleaning up on every deployment to all of our environments simultaneously!
- Discord Update.
  - We’ve had an increase of 99.6% in our visitor count compared to last month, wow! Thanks for checking in with us.
  - Our total messages sent in the server has also increased this month by 25.6% compared to last month! Let’s see if we can keep conquering these numbers!
  - Discord will definitely be thanking us because a total of 95% of our new members for this month are also new to Discord! We’re so touched you’re signing up just for us! (Even if you’re not…)

## Closed Tickets This Month

- [[Bug] Composite Map Editor hangs when uploading images >32mb.](https://mythictable.us20.list-manage.com/track/click?u=34d56cd42f708cf3153691d42&id=5c5f5221af&e=9e91d9e406)

- [[Bug] Invalidating a single-macro token's macro text causes delete when cancel is pressed.](https://mythictable.us20.list-manage.com/track/click?u=34d56cd42f708cf3153691d42&id=ca02e324c1&e=9e91d9e406)

- [[Bug] Hitting delete while editing a character will delete the token from the board.](https://mythictable.us20.list-manage.com/track/click?u=34d56cd42f708cf3153691d42&id=f8dd14ba5b&e=9e91d9e406)

## What we are working on now

- [New Epic: Web!](https://mythictable.us20.list-manage.com/track/click?u=34d56cd42f708cf3153691d42&id=bda1e0da06&e=9e91d9e406)

  - We’ve created a new epic that will help us organize our web development goals. These goals are split into two phases: Web: Stage 1 and Web: Stage 2. Stage 1 will focus on updating our currently available site to ensure it’s displaying as up-to-date information as possible. Stage 2 will begin our journey toward our new web presence. Keep an eye out for that!

- [Kickstarter Rewards](https://mythictable.us20.list-manage.com/track/click?u=34d56cd42f708cf3153691d42&id=2a739d9fc8&e=9e91d9e406)

  - We’re focusing hard on Kickstarter rewards, some are code-heavy and take time. Others are personnel heavy! We’re working to get everyone the access they should have as soon as possible.

- Our Roadmap
  - Well, it’s extremely out of date, and we’re committed to updating that moving forward. We hope to have a more clearly defined roadmap soon. Until then, we’re taking our previously displayed roadmap down.

## On the Horizon

    [Team Mythic Table](https://mythictable.us20.list-manage.com/track/click?u=34d56cd42f708cf3153691d42&id=0a0ea49d54&e=9e91d9e406) will once again be participating in another [Sleep Out Vancouver](https://mythictable.us20.list-manage.com/track/click?u=34d56cd42f708cf3153691d42&id=5bc7800081&e=9e91d9e406) with the aim of raising awareness and funds for [Covenant House](https://mythictable.us20.list-manage.com/track/click?u=34d56cd42f708cf3153691d42&id=a4ae806c26&e=9e91d9e406), BC. Quoted from their website: "Covenant House Vancouver’s purpose is to serve all youth, with absolute respect and unconditional love, to help youth experiencing homelessness, and to protect and safeguard all youth in need."

Their [core principles](https://mythictable.us20.list-manage.com/track/click?u=34d56cd42f708cf3153691d42&id=610689d806&e=9e91d9e406) include: Immediacy, Sanctuary, Structure, Value Communication, and Choice—all of which are principles we hold with high regard.

Please support us by sharing the team's link: [https://bit.ly/mtsleepout](https://mythictable.us20.list-manage.com/track/click?u=34d56cd42f708cf3153691d42&id=81228b24f5&e=9e91d9e406)

## Want to get involved?

You're in luck! We're always looking to bolster the Mythic Table ranks. We need QA, digital illustrators, developers and more. To learn more about us and where or how you might contribute, join us in [Discord](https://mythictable.us20.list-manage.com/track/click?u=34d56cd42f708cf3153691d42&id=96f0bf0de7&e=9e91d9e406).

## Financial Report

For all the details on our financials view our [full report on the website](https://mythictable.us20.list-manage.com/track/click?u=34d56cd42f708cf3153691d42&id=667511ea8c&e=9e91d9e406).

Enjoy our project and want to be part of its progress? Consider our [Patreon](https://mythictable.us20.list-manage.com/track/click?u=34d56cd42f708cf3153691d42&id=2e37b3449f&e=9e91d9e406).

Thanks for joining us around the table and 'til we meet—Adventurer—Roll High!

× Shield and the Mythic Table Team

Pssst! Community is everything to us.
Come chat with the development team on [Discord](https://mythictable.us20.list-manage.com/track/click?u=34d56cd42f708cf3153691d42&id=db29fa737a&e=9e91d9e406).
