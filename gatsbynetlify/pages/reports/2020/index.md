---
date: '2020-11-30'
title: 'Reports for the Year 2020'
summary: 'A list of all Mythic Table reports for the Year 2020'
---

If a month has passed and you don't see a report added here, please wait, it will be added shortly. Thanks for your patience. If you have any questions about this please use the contact information below to reach out.

The reports are further arranged by months. To access a particular report, please choose the month below:

* [Report for month of January](/reports/2020/01/)
* [Report for month of February](/reports/2020/02/)
* [Report for month of March](/reports/2020/03/)
* [Report for month of April](/reports/2020/04/)
* [Report for month of May](/reports/2020/05/)
* [Report for month of June](/reports/2020/06/)
* [Report for month of July](/reports/2020/07/)
* [Report for month of August](/reports/2020/08/)
* [Report for month of September](/reports/2020/09/)
* [Report for month of October](/reports/2020/10/)
* [Report for month of November](/reports/2020/11/)
* [Report for month of December](/reports/2020/12/)
