---
date: '2020-07-31'
title: 'Mythic Table July 2020 Report'
summary: 'Where we are and where we are going'
---

July has been a great month for Mythic Table

## TL;DR

* **Wins**: We have had several new people join in developing Mythic Table this month! And we are so close to releasing our First Playable! So so close!
* **Challenges**: Time differences are making it tough to collaborate
* **Plans**: To keep going and ramp up our efforts before launch!

## Development

We have been making some huge progress toward a First Playable (MVP) version of Mythic Table. As we start finishing up some of the last features for the First Playable, we’re paying special attention to polish and usability. A UI designer is working with us to aid in that.

Additionally, we have been working hard on improving our appearance and presentation so that new users can easily learn all they would want to know about us. One way to do this was to edit the welcome email that goes out when a subscriber signs up for our Newsletter, which now gives some clear next steps that they can take.

We want new users to know how they can support us. Whether this support be Patreon contributions, sharing our story on Social Media, or joining our development team! Our community size is currently 1832 – our target is 6000 – but we expect these improvements will make a positive impact on our community numbers.

Finally, the team has been hard at work planning and revising the UI for an improved User Experience and finishing up some of the bigger tasks like Map and Campaign Management.

### Chart 1: First Playable Feature Burndown

![Burndown](../../../images/reports/2020/07/burndown.png)

First of all, you might notice a huge jump in issues. This spike represents a number of high priority items we’ve recently committed to in order to help improve the overall user experience and includes things like a Guest User Flow, Default Campaign for tutorials, Drawing tools and just an overall UX pass.

Our development has been slower this month from last. We saw our volunteers put in about 215 hours in total compared to 335 last month (this may simply be an issue of unreported hours from the team). This effort includes onboarding, design work, website revisions, but a large portion of it was in development and operations work. Even though it has been a slow month, as of today we have just 4 items left to be completed before we are ready to launch the First Playable!

### Financials

Our financial plan looks about the same as it did in our last report. Our Income for the month is $123.81, our Cost is -$276.36, and we are showing a negative balance of -$5,702.81. 

## Plans

PLANS
Our short-term goal is Sustainability, to be achieved by releasing our First Playable and following up with community growth. Our long-term goal is to generate enough interest in Mythic Table to support full-time development. 

The website improvements are very important to achieving our goals. We want to present a polished,professional looking site before we release our First Playable.
In the meantime, we are holding “Town Hall” style meetings, design sprints, and weekend hackathons for anyone that can spare the time. If you are interested in being involved with Mythic Table’s development or helping us out in another capacity, please visit this page to learn how you can help: https://www.mythictable.com/welcome/why

Thanks so much for your continued support!

Sarah Kilby<br/>
Director of Communications<br/>
Acting Director of Production<br/>
Mythic Table Foundation



