---
date: '2020-02-29'
title: 'Mythic Table February 2020 Report'
summary: 'Where we are and where we are going'
---

We are excited to bring you our latest Mythic Table Report!

## TL;DR
* **Wins**: We have been continuing to hold weekend hackathons to much success! We are also working on smoothing onboarding for new contributors… & Mythic Table Stickers are even in the works!
* **Challenges**: Collaborating across time zones with varying schedules
* **Plans**: Keep on keeping on!
* **Action Items**: Working on securing Not For Profit Grants for Mythic Table

## Development Progress
The following is a shortlist of our development achievements this month:
1. We completed our session zero using Mythic Table 
1. Added Google Analytics into the app
1. Added tokens for each character and some of the enemies 
1. Focus remained on Campaign Management

As mentioned, the first demo of the campaign management feature was successful. This means we are one more step closer to bringing Mythic Table to an audience. 

## The Numbers

As Community Growth remains deprioritized, our Critical Number is still going to be the number of Open issues in the First Playable Milestone. This milestone is public and it can be found here: https://gitlab.com/mythicteam/mythictable/-/milestones/2.

### Chart 1: First Playable Feature Burndown

![Burndown](../../../images/reports/2020/02/burndown.png)

What we are seeing here is that 6 new tasks were added in February and 6 were completed. We’re trending to complete 2 more within the next couple of days, but they are not finished yet.  

## Forecast and Trend

We've pushed our release back two months in our forecast to account for slower development than we expected. The result is that we do not expect to reach a revenue-generating stage until July.

### Chart 2: Overall forecast vs. trend 

![Financials](../../../images/reports/2020/02/financials.png)


We’ve simplified this chart from our previous reports because we found that the forecasts were not very helpful.  Instead, we are focusing on the reality of our progress.

### Adaptations since January:
* Modified for Patreon account in our income and reduced server costs.
* Pushed back release dates.
* Removed revenue generation from First Playable and moved that to Release 0.2.

## Plans 

We will be continuing the weekend hackathons. All of our developers are encouraged to find the time to put towards this effort, where possible. We are also going to take advantage of any grants available to us as a Not-for-Profit. We will post updates on that in our next Report!

If you would like to send us your feedback on what we are doing please post on our Facebook page, here: https://www.facebook.com/mythictable/ or email myself at sarah@mythictable.com and I will get back to you shortly. 

As always, thank you for taking the time to read this Report!

--Sarah K<br/>
Mythic Table Community Champion<br/>
Mythic Table Foundation Director of Communications<br/>
