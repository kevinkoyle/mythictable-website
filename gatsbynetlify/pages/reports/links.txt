---
date: '2020-06-18'
title: 'Hotlinks'
summary: 'Hotlinks reference page'
---

These are the places where the files from this folder are hotlinked except the ones linked to the index.md file in this folder. These are referenced here in case one needs to move or rename one of the files in here.

index.md -> /gatsbynetlify/src/components/nav.js
