---
date: '2021-05-01'
title: 'Mythic Table April 2021 Report'
summary: 'Where we are and where we are going'
---
## The Mythic Table April 2021 Report!
This month has been intense with the Kickstarter well underway and more development happening alongside.  We are just so blown away by everyone’s support.  The entire community has been working hard, sharing maps, tokens, assets, images using Mythic Table, even streaming using Mythic Table!  We’ve had people donate advertising space, donate to fund advertising campaigns and share about us on social media!

From the bottom of our Mythic Hearts, we cannot say thank you enough!  Without you, we wouldn’t have gotten so far.  Thank you!

## A Glance At This Month

* We hit the front page on Kickstarter! And we’re only 2 platinum pledges off our goal!
* Unfortunately, we were so busy with the Kickstarter we forgot to plan the April Milestone!
* We've been working hard on improving the grid experience, making it more flexible, but it was a complicated change and we are still working on it.
* Our user base increased by another 16% to 15807!!

### Achievements
* Our Kickstarter is almost funded!  We’re just 2 platinum pledges from a fog of war, a better grid experience and possibly rotating tokens!
* We launched a Mythic Table FAQ page on our website to answer most of your questions! https://www.mythictable.com/faq/
* We’re now at 15,807 unique accounts
* We saw an average of 236 of you playing games on Mythic Table every day!

### Fixes
* We removed some unnecessary scroll bars that appeared from time to time when editing a campaign
* We resolved an outage that caused guest to be unable to login to a campaign

### Plans for May
* We’ll be wrapping up the Kickstarter campaign and start planning a refreshed roadmap to deliver features to you
* We will be committing to delivering a better grid experience
* We will be committing to GM only controls
* We will start scoping fog of war and design it to lay the groundwork to deliver it in upcoming months

## Kickstarter Update
The Kickstarter launched on 30th March with great success!  We hit 30% of our goal within the first few days and have been diligently working on improving our outreach in the market with great success!  We now have 1,035 followers on the project, which is TRIPLE the size of our community!  We’re only $2000 away from our goal now so please keep on sharing!  We really don’t want you to miss out on the opportunity for full integration with the Open Gaming Store and World Anvil!!!!!  Pledge now on the link below!
https://www.kickstarter.com/projects/mythictable/mythic-table/

For those who have already pledged, Thank You so much!!  We have been blown away by everyone’s support!  In particular, we want to thank RedDragon, Raznag and Zaezar for donating their maps to help us with the Kickstarter page and our trailer.  Also much love to Moebius Adventures and The Roll Playerz for their shoutouts on stream and social media.  Also, an enormous thank you to everyone who has been lovingly giving us feedback on how to improve the kickstarter page!  As you all know, it was our first time running a campaign and based on community feedback, we redesigned the Kickstarter page with a magnificent result of increasing our conversion rate from 2% to 12%!  The industry standard when hiring professionals is 5%!! This was all you guys!!

## Thank you and farewell to José
This month, we also said a sad farewell to a very valued member of the Mythic Team, José Bonilla.  José has been with Mythic Table for 18 months as a Devops leader, overseeing both our development and the operations of our cloud service servers.  José will be sorely missed. And we wish him all the best in his future endeavors!

As a result, our leadership responsibilities have been reshuffled as follows:

*Production and Planning:* Linda Spencer</br>
*Development and Operations:* Marc Faulise</br>
*Quality Assurance:* Barry deFreese</br>
*Community:* Sarah Kilby</br>
*Design:* Lucas Teng</br>

## Financial Report
For full details on our operations including our financials, please refer to our [Financial Reporting Page](/finance/index)

And that’s all for this month folks!  Thank you so much for your ongoing support!  Be sure to follow us on Kickstarter, Twitter and Instagram for more regular updates.  And share us with your friends so we can hit those goals!

Until next time….
Roll High

The Team
Mythic Table
