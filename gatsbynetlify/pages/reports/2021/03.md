---
date: '2021-04-01'
title: 'Mythic Table March 2021 Report'
summary: 'Where we are and where we are going'
---
## Mythic Table March 2021 Report
This month the main focus was to deliver community requested features and prepare Mythic Table for the crowdfunding campaign via Kickstarter. The main requested feature from the community was functionality to align the grid to existing grids on uploaded maps. Development work on this feature did commence during the month. Unfortunately, the work was not completed due to reduced developer availability as a result of the focus on fundraising and marketing for the Kickstarter campaign. This work will be deferred to the next milestone for the time being.

The Kickstarter campaign started well on 30th March, 2021 and at the time of writing, the Kickstarter campaign was at almost $9000 funded with 156 backers

## A Glance At This Month

### Delivered Functionality
* Improved dice rolling
* Macro enabled contextual rolling
* Toolbar improved user interface
* Bug fix: token misalignment
* Bug fix: uploading gifs

### Kickstarter and Marketing Initiatives
* Mythic Table Trailer
* Kickstarter Campaign Video
* Website Update to promote Kickstarter
* Kickstarter campaign page development

### Business Initiatives
* Ongoing partnership with Open Gaming Network
* Ongoing partnership with World Anvil
* Liaison with Community Creators

### Ongoing Initiatives (Not Completed This Month)
* Grid Alignment in MTVTT
* Content Management System upgrade to website

## Financials
For full details on our operations including our financials, please refer to our [Financial Reporting Page](/finance/index)

## Mythic Table in the Media
It has been quite exciting and encouraging to see Mythic Table and our Kickstarter campaign gain media attention.  It has also been great to see some of your creations from our creator community get featured.  We are so grateful for your contribution and support.  Mythic Table would not be what it is today without you!  Check out these articles that featured Mythic Table!

* https://www.geeknative.com/130064/open-source-mythic-table-launches-kickstarter-for-free-vtt/
* https://www.thegamer.com/mythic-table-promises-to-be-virtual-tabletop-of-the-future/

It has been an absolute pleasure and an honour to serve you, our community, to bring you the best free, open source virtual tabletop that the world has ever seen.  For those of you who are taking holidays this Easter season, on behalf of the Mythic Team, I wish you the best of holidays with your family and friends.  For those who are in lockdown, or not taking a break, we’ll catch up with you on discord and remember...Roll High!

Linda Spencer<br>
Producer<br>
Mythic Table Foundation
