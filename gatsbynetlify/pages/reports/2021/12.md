---
date: "2021-12-31"
title: "Mythic Table December 2021 Report"
summary: "Where we are and where we are going"
---

## The Mythic Table December 2021 Report!

Greetings, Adventurer!

We hope that your December has been full of peace and exceptional escapades. For Mythic Table, this month has been a whirlwind of magic and wonder: For the first month since launching, we've reached our first Patreon milestone! We have the continued support of our generous Patrons to thank for helping us accomplish that - If you see one around, please remember to express your gratitude.

We've also found a new victim—er, I mean…developer. Welcome to HonorSpren! Many of our volunteers have taken a slight break this holiday season to spend time with loved ones, enjoy the vast offerings life has to give, and recharge their batteries, but everyone’s starting to filter back in with a full charge as we all barrel toward Mythic Table, 2022!

### Achievements This Month

- Patreon Milestone Reached!
  - We have attained our bottom-line infrastructure goal intended to cover Mythic Table's basic running costs.
- Social Achievements:
  - Discord Boost Level 2! Thank you to our generous Discord community boosters for helping us reach level 2! Only 6 more boosts to go until we achieve boost level 3 at which point we’ll be able to apply for a vanity URL.
  - Discord new member retention is up 52.9%! Wow! We appreciate you joining the conversation and love to hear your feedback, thoughts, suggestions, etc.!
- Successfully celebrated Hanukkah, Christmas, and Kwanzaa and gave the team some much-needed downtime. Good news, though—we're back in the fray! Hurray, hurray!
- Continual conceptualization of how to serve you - our incredible community - better. Reflection on the year past and hopes for the future.

### Closed Tickets This Month

Our less-than-padded holiday team was unable to successfully close any tickets this month. We review each one as it’s submitted, though, and now that we’re back in full-force, we can’t wait to get to work tackling those once again!

## What we are working on now

New year; new goals! Despite being very busy, our focus has continued on the attainment of the Kickstarter goals. No additional features have been completed in the month of December, but we’re working hard on fulfilling some specific open issues by the end of January.

## Want to get involved?

You're in luck! We're always looking to bolster the Mythic Table ranks; QA, digital illustrators, developers and more. To learn more about us and where or how you might contribute, join us in [Discord](https://discord.gg/c4bEWDQ).

## Financial Report

For full details on our operations including our financials, please refer to our [Financial Reporting Page](/finance/index)

If you like what we are doing and want to support us, consider our [Patreon](https://www.patreon.com/mythictable).

Thanks again for your continued support and until next we meet—Adventurer—Roll High!

Sarah K
Mythic Table

Pssst! Community is everything to us. Come chat with the development team on Discord, here:
[https://discord.gg/gTg3BSzNWv](https://discord.gg/gTg3BSzNWv)
