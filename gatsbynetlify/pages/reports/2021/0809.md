---
date: "2021-09-30"
title: "Mythic Table August and September 2021 Report"
summary: "Where we are and where we are going"
---

## The Mythic Table August and September 2021 Report!

## Achievements since August

- Composite Maps!
  - As a user, you can create maps composed of multiple images
    - Map area contains no image when it's first created
    - Images can be dragged onto the map from the desktop
    - Map images can be moved, scaled, and rotate
    - Map images z-ordering can be rearranged
- Token Visibility!
  - As a DM or a token owner, you can toggle the token visibility
    - This visibility state is clear and recognizable
    - Invisible tokens can only be seen by owning player or with GM permissions

### What we are working on now

- Tagging System
- Rating
- Object Improvements
- UI Improvements
- Sharing, Searching
- Fulfilling the rest of our Kickstarter rewards such as:
  - Sending out some remaining World Anvil Discounts
  - Rolling out the Open Gaming Store Discounts
  - Other rewards for high and low pledges

### World Anvil Discount Update

We sent out the first wave of World Anvil codes on July 15, 2021! These codes give access to a discount on the World Anvil, Grandmaster Tier at [https://www.worldanvil.com/pricing](https://www.worldanvil.com/pricing) These were given out as a thank you to those who pledged $25 or more toward our Kickstarter! If you haven’t gotten your code please let us know. We should be able to resend your code.

## Financial Report

For full details on our operations including our financials, please refer to our [Financial Reporting Page](/finance/index)

If you would like to support us beyond our Kickstarter, we would love for you to check us out on Patreon, here: [https://www.patreon.com/mythictable](https://www.patreon.com/mythictable)

Until next time….
Roll High

Sarah K
Mythic Table

Pssst! Community is everything to us. Come chat with the development team on Discord, here:
[https://discord.gg/gTg3BSzNWv](https://discord.gg/gTg3BSzNWv)
