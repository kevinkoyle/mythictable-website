---
date: "2021-06-30"
title: "Mythic Table June 2021 Report"
summary: "Where we are and where we are going"
---

## The Mythic Table June 2021 Report!

## A Glance At This Month

- How we are using the funds we raised from our Kickstarter
- Achievements:
  - Finished the Grid experience
  - Finished the Fog of War
  - Many more people are trying MT
- Plans
  - Rolling out rewards for those who backed our Kickstarter campaign.
  - Pushing towards fulfilling the rest of our Kickstarter goals

### Kickstarter Update

Hey! As you may have heard, our Kickstarter ended on May 22nd, and with help from our community we surpassed our first goal of $25,000 and arrived at a total of $31,836! As planned, we have begun using that money to contract two of our volunteer developers, Logan, and Keith! With their help we are speeding up development of Mythic Table features substantially! So thank you for pledging and sharing our story!!

So you pledged to our Kickstarter. What can you expect to receive? Discounts on the Open Gaming Store and World Anvil, which will be activated soon! If you haven't received any communication from us after pledging, please make sure to check your email which contains a short survey. Once we receive that survey we will be emailing you a unique code to claim your Rewards ASAP. Thanks for your patience! We also hope you will join our community and Devs on Discord. There is a backers-only channel for you! Follow this link: [https://discord.com/invite/c4bEWDQ](https://discord.com/invite/c4bEWDQ)

If you would like to keep supporting us beyond the kickstarter, we would love for you to check us out on patreon! HERE: [https://www.patreon.com/mythictable](https://www.patreon.com/mythictable)

### Achievements This Month

- We’re now at 21392 unique player accounts since launching
- We saw an average of 175 of you playing games on Mythic Table every day!

### Plans for July

We’ll be rolling out rewards for those who backed our Kickstarter campaign. We’ll also be pushing towards fulfilling our Kickstarter goals starting with exciting features such as GM tools, Composite Maps and better asset management.

## Townhall for This Upcoming Month

As you may know, we hold a Townhall meeting once per Month. The next one will be held on July 3rd. This format gives us the chance to talk about what we are doing, such as Announcements, Kudos etc. This also gives you the opportunity to chat with us live, and we can answer some of your questions! Be gentle! We will post out the link to join us in this call via our social media once we go live!

## Financial Report

For full details on our operations including our financials, please refer to our [Financial Reporting Page](/finance/index)

## Thanks!

And that’s all for this month folks! Thank you so much for your ongoing support!

Until next time….
Roll High

Sarah K
Mythic Table

Pssst! Community is everything to us. Come chat with the development team on Discord, here:
[https://discord.gg/gTg3BSzNWv](https://discord.gg/gTg3BSzNWv)
