---
date: '2019-08-08'
title: 'Mythic Table August 8th 2019 Report'
summary: 'Where we are and where we are going'
---

Hi everyone,

Welcome to the very first Mythic Table Report.  In this report, you will get access to our forecast and our progress.  This report will go into our finances because finances are important, but our main focus will be on our Critical Number.  Great Game of Business introduces the idea of a Critical Number.  I speak more about this book here: https://mythictable.com/org/ggob/.  I highly recommend it, especially if any of you are managers or owners of a business.

In these reports, I will address all of you as though you are stakeholders of Mythic Table, which you are, in effect.  You've risked your privacy and given a stranger your email address because you believe in this product.  You want Mythic Table to succeed so you can use it for your games.  You might not have money on the line, but you have your hopes and your time already invested.  My job is to make sure you continue to consider this a good investment.
 
Our first Critical Number is Newsletter Subscriptions.  The number of active subscribers is essential because we can directly infer the success of our first Kickstarter.  Below is a graph that shows where we are and where we hope to be by the time the Kickstarter campaign is ready.

![Community](../../../images/reports/2019/08/community.png)

These numbers are based on the following assumptions.
I can grow the community at a rate of 30-40 people per day.
25% of our subscribers will contribute.
We can count on an average contribution of $20 per contributor.
We will need at least $24,000 to fund the contract for the first playable.
As you can see, it will take us some time to get to these numbers. However, I am willing to launch the Kickstarter before we get all the way to our target.  The Kickstarter itself will help to generate interest.  The worst-case scenario is that we don't quite meet our goals, and we dip into the red again.

Now we will discuss the financial report.  The following chart shows where we are now and to where want to go.  It is too early to forecast any trends, so a lot of these figures are based on best guessing.

![Forecast](../../../images/reports/2019/08/forecast.png)

The dotted lines indicate where we want to be, and the solid lines indicate where we are trending.  This is the first report.  It is a little rough. As I produce more of these, I will find a rhythm and a style that works well. For the time being, please let me know if there is anything you don't understand.
So, the numbers above are based upon the following assumptions:
We are starting with a debt of $2500, which is our running infrastructure and marketing cost.
The Kickstarter is planned to finalize in November at which time we'll enlist a single contractor to build the First Playable.
Once the first playable is launched, we'll be able to accept voluntary subscriptions to support the project.
25% of our users will subscribe at an average rate of $5 per month.
We will shoot for Release 0.2 after the First Playable with a single developer keeping our development costs steady.
Release 0.2 will be ready by March of 2020, and we'll ramp up to 3 developers at that time in an attempt to take on more feature work. 
The financial report is a lot of information.  I will do my best to make this more concise in the future.  In the meantime, I encourage you to respond to me directly with questions.  These questions will help me surface the information relevant to you.

Finally, I would like to discuss the idea of developer contributions.  During my announcements to Reddit today, I received a significant number of requests to assist with the development effort.  More than I expected.  This is fantastic.  I don't know how much it will impact my plans.  My gut is telling me this contribution will be minimal unless I can find a good lead from the group.

So, what I'd like to do for the time being is to get you guys onto our slack channel.  For now, I would like to limit this to developer wishing to participate.  We can use this channel to work out the details of how this will look.  If you're interested in joining, please get 1n touch.
