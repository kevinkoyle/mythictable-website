---
date: '2020-06-18'
title: 'Reports for the Year 2019'
summary: 'A list of all Mythic Table reports for the Year 2019'
---

We are still in the process of bring the old content oer to the new site. Some reports still need to be added. Thanks for your patience. If you have any questions about this please use the contact information below to reach out.

The reports are further arranged by months. To access a particular report, please choose the month below:

* [Report for month of August](/reports/2019/08/)
* [Report for month of September](/reports/2019/09/)
* [Report for month of October](/reports/2019/10/)
* [Report for month of November](/reports/2019/11/)
* [Report for month of December](/reports/2019/12/)
